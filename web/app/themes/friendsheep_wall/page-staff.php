<?php

/**
 * The template for displaying all pages.
 *
 * Template Name: Staff Members
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 */

namespace App;

use App\Http\Controllers\Controller;
use App\PostTypes\Staff;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

class PageStaffController extends Controller
{

    public function handle()
    {
        $context = Timber::get_context();
        $post = new Post();

        // Add all data to the context for outputting
        $context['post'] = $post;
        $context['title'] = $post->title;
        $context['content'] = $post->content;

        $staffWithPraise = $this->getStaffWithPraise();
        $context['staff'] = $staffWithPraise;

        return new TimberResponse('templates/users.twig', $context);
    }

    public function getStaffWithPraise()
    {

        $staffWithPraise = [];

        $staff = Timber::get_posts([
            'post_type' => 'staff',
            'posts_per_page' => -1,
            'orderby' => [
                'date' => 'DESC'
            ]
        ]);

        foreach ($staff as $key => $value) {

            $staffWithPraise[] = [
                'staff' => $value,
                'praise' => $this->getPraise($value->ID)
            ];
        }

        return $staffWithPraise;

    }

    public function getPraise($staffID)
    {

        $praiseLength = 0;

        $praises = Timber::get_posts([
            // Get post type project
            'post_type' => 'post',
            // Get all posts
            'posts_per_page' => -1,

            // Gest post by "graphic" category
            'meta_query' => [
                [
                    'key' => 'praise_to',
                    'value' => $staffID,
                    'compare' => 'LIKE'
                ]
            ],
            // Order by post date
            'orderby' => [
                'date' => 'DESC'
            ]
        ]);

        foreach ($praises as $key => $value) {
            $praiseLength++;
        }

        return $praiseLength;

    }


}

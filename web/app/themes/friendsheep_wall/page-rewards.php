<?php

/**
 * The template for displaying the Reweards pages.
 *
 * Template Name: Rewards
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

class PageRewardsController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $post = new Post();

        // Add all data to the context for outputting
        $context['post'] = $post;
        $context['title'] = $post->title;
        $context['content'] = $post->content;

        $context['rewards'] = Timber::get_posts([
            'post_type' => 'reward',
            'posts_per_page' => -1,
            // 'meta_query' => [
            //     [
            //         'key' => 'praise_to',
            //         'value' => $post->ID,
            //         'compare' => 'LIKE'
            //     ]
            // ],
            'orderby' => [
                'date' => 'DESC'
            ]
        ]);

        return new TimberResponse('templates/rewards.twig', $context);
    }
}

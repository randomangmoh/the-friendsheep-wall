<?php

/**
 * The Template for displaying all single posts
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

class SingleStaffController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $post = new Post();

        // Add all data to the context for outputting
        $context['post'] = $post;
        $context['title'] = $post->title;
        $context['content'] = $post->content;

        $context['posts'] = Timber::get_posts([
            // Get post type project
            'post_type' => 'post',
            // Get all posts
            'posts_per_page' => -1,
            // Gest post by "graphic" category
            'meta_query' => [
                [
                    'key' => 'praise_to',
                    'value' => $post->ID,
                    'compare' => 'LIKE'
                ]
            ],
            // Order by post date
            'orderby' => [
                'date' => 'DESC'
            ]
        ]);

        return new TimberResponse('templates/user.twig', $context);
    }
}

<?php

define('DONOTCACHEPAGE', true);

use App\Http\Lumberjack;

// Create the Application Container
$app = require_once('bootstrap/app.php');

// Bootstrap Lumberjack from the Container
$lumberjack = $app->make(Lumberjack::class);
$lumberjack->bootstrap();

$GLOBALS['home'] = home_url() . '/';
$GLOBALS['assets'] = '/assets/';
$GLOBALS['current_user'] = wp_get_current_user();

// Import our routes file
require_once('routes.php');

// Set global params in the Timber context
add_filter('timber_context', [$lumberjack, 'addToContext']);
add_filter('use_block_editor_for_post', '__return_false', 10);

// Enqueue fonts

function addStylesheets() {
    wp_enqueue_style( 'googleFonts', 'https://fonts.googleapis.com/css2?family=Anton&family=EB+Garamond:wght@400;500', [], null );
    wp_enqueue_style( 'global', get_template_directory_uri() . $GLOBALS['assets'] . 'css/global.css', false );
}
add_action( 'wp_enqueue_scripts', 'addStylesheets' );


// OOTB title character limit
function max_title_length( $title ) {
    $max = 30;
    if( strlen( $title ) > $max ) {
        return substr( $title, 0, $max ). " &hellip;";
    } else {
        return $title;
    }
}

add_filter('the_title', 'max_title_length');


// add_action( 'publish_post', 'send_notification' );
// function send_notification( $post_id ) {
//     $post     = get_post($post_id);

//     // print_r($post);
//     // die();
//     // // $post_url = get_permalink( $post_id );
//     // // $post_title = get_the_title( $post_id );
//     // // $author   = get_userdata($post->post_author);
//     // // $subject  = 'Post publish notification';
//     // // $message  = "Hello,";
//     // // $message .= "<a href='". $post_url. "'>'".$post_title."'</a>\n\n";
//     // // wp_mail($author->user_email, $subject, $message );
// }

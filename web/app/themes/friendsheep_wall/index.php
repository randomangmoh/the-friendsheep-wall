<?php

/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

class IndexController extends Controller
{
    public function handle()
    {

        acf_form_head();

        $context = Timber::get_context();
        $context['posts'] = Post::all();
        $context['users'] = Timber::get_posts(['post_type' => 'users']);

        $context['post_args'] = [
            'post_id' => 'new_post',
            'post_title' => true,
            'post_content' => false,
            'updated_message' => 'Thanks for submitting!',
            'form_attributes' => [
                'class' => 'submission-form',
                'placeholder' => 'Enter'
            ],
            'new_post' => [
                'post_status' => 'publish',
                'post_type' => 'post',
            ],
            'submit_value' => 'Submit some praise'
        ];

        acf_form_head();

        return new TimberResponse('templates/home.twig', $context);
    }
}
